<?php
ini_set('display_errors', 0);

require_once 'application/core/view.php';
require_once 'application/core/controller.php';
require_once 'application/core/route.php';
	
// get site directory
$site_dir = str_replace($_SERVER['DOCUMENT_ROOT'], "", str_replace(DIRECTORY_SEPARATOR, '/', __DIR__));

Route::start();