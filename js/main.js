$(function(){
	// text slideshow on home page with radiobuttons	
	var arr = [
		'Tempora mutantur et nos mutamur in illis.',
		'Always borrow money from a pessimist. He won’t expect it back.',
		'It is better to have a permanent income than to be fascinating.'
	];
	var radio = $("input[name='home-btns']");
	radio.each(function(i){
		$(this).click(function(){ 
			$('#txt').val(arr[i]);
		});
	});	
	
	radio[0].click();
	slideText();

	function slideText(){		
		var timer = setInterval(function(){
			var i = $("input[type='radio']").index($("input[name='home-btns']:checked"));
			if (i >= radio.length - 1) i = -1;
			radio[++i].click();
		}, 4000);
	}

	// moveUp button functionality
	$(window).scroll(function(){
		$(window).scrollTop() !== 0 ? $('#moveUp').show() : $('#moveUp').hide();
	});
	
	// menu button functionality
	$('#menu_btn, .m-small li').click(function(){	
		$('#menu_ul').slideToggle(500);
	});
			
	// animated skills bar
	$(window).bind('scroll', function(ev){
		var  oTop = $('.first-inner .my-skills').offset().top - window.innerHeight,
			oBottom = $('.second-inner .my-skills').offset().top;
		if ($(window).scrollTop() > oTop && $(window).scrollTop() < oBottom) {
			$('.first-inner .my-skills li').each(function(){
				$(this).find('.skillbar-bar').animate({
					width:$(this).attr('data-percent')
				},2000);
			});
			$(window).unbind('scroll', arguments.callee);
		}		
	});
	
	// animated counter 
	$(window).bind('scroll', function(){		
		var  oTop = $('.more').offset().top - window.innerHeight;
		  oBottom = $('.contact').offset().top;
		if ($(window).scrollTop() > oTop && $(window).scrollTop() < oBottom) {
		$('.counter-value').each(function() {
				var $this = $(this),
					countTo = $this.attr('data-count');
				$({
					countNum: $this.text()
				}).animate({
						countNum: countTo
					},
					{
						duration: 2000,
						easing: 'swing',
						step: function() {
							$this.text(Math.floor(this.countNum));
						},
						complete: function() {
							$this.text(this.countNum);
						}
					});
			});
			$(window).unbind('scroll', arguments.callee);
		}
	});
	
	// animated circle bar 	
	$(window).bind('scroll', function(){		
		var  oTop = $('.second-inner .my-skills').offset().top - window.innerHeight,
			oBottom = $('.gallery').offset().top;
		if ($(window).scrollTop() > oTop && $(window).scrollTop() < oBottom) {
			var angles = [];
			$('.z3').each(function(i){
				angles[i] = parseFloat($(this).text()) * 3.6;
			});
			$('.z0').each(function(i){
				if (angles[i] < 180) {
					$(this).animate({
						rotate : angles[i] + 'deg'
					},{
							duration: angles[i] * 6, 
							easing: 'linear'
					});
				}
				else {
					$(this).animate({
						rotate : '180deg'
					},{
						duration: 180 * 6,
						easing: 'linear',
						complete: function() {
							$($('.z2')[i]).css({display: 'block'});
							$($('.z2')[i]).animate({
								rotate : angles[i] - 180 + 'deg'
							},{
								duration: (angles[i] -180) * 6,
								easing: 'linear'
							})
						;}
					});					
				}
			});
			$(window).unbind('scroll', arguments.callee);
		}		
	});
	
	// gallery slide show
	var images = $('.images');
	var curIndex;
	
	// hide thumbnails / unhide photoviewer
	function photoviewer(el){
		$('.thumbnails').css('display', 'none');
		$('.photoviewer').css('display', 'inline-block');		
		curIndex = parseInt($(el.currentTarget.innerHTML)[0].attributes[2].value);
		$('.current-img').attr('src', images[curIndex].src);
		if (curIndex < images.length - 1) {
			$('.next-img').attr('src', images[curIndex + 1].src);					
		}
		else if (curIndex == images.length - 1) {
			$('.next-img').attr('src', images[0].src);	
		}
		if (curIndex > 0) {
			$('.prev-img').attr('src', images[curIndex - 1].src);
		}	
		else if(curIndex == 0) {
			$('.prev-img').attr('src', images[images.length - 1].src);
		}				
		
		var layerHeight = 460; //parseFloat($('.current-img').css('height'));
		$('.back').css('height', layerHeight + 40);
		$('.front').css({
			height: layerHeight,
			clip: 'rect(0,900px,' + layerHeight + 'px,0)'
		});
		$('.view-border').css('height', layerHeight);
	}
	
	function photoviewerSwitch() {
		if ($(window).width() > 674) {
			$('.thumbnails a').bind('click', photoviewer);		
			$('.close-btn').click(closePhotoviewer);
		}
	}
	
	function closePhotoviewer(){
		$('.thumbnails').css('display', 'inline-block');
		$('.photoviewer').css('display', 'none');
	}
			
	photoviewerSwitch();
	$(window).resize(function() {
		if ($(window).width() < 675) {
			$('.thumbnails a').unbind('click', photoviewer);
			closePhotoviewer();
		}
		else		photoviewerSwitch();
	});
	
	// prev/next events
	$('.fa-angle-left').click(function(){
		$('.current-img').animate({
			left: '+=900'
		}, {
			duration: 1000,
			complete: function(){
				if (curIndex > 1) {
					curIndex--;					
				}
				else if (curIndex == 1) {
					curIndex = 0;
				}
				else if (curIndex == 0) {
					curIndex = images.length - 1;
				}		
				$('.current-img').attr('src', images[curIndex].src);
				$('.current-img').css({left:'0px'});
			}
		});	
		$('.prev-img').animate({
			left: '+=900'
		}, {
			duration: 1000,
			complete: function(){				
				if (curIndex < images.length - 1) {
					$('.next-img').attr('src', images[curIndex + 1].src);					
				}
				else if (curIndex == images.length - 1) {
					$('.next-img').attr('src', images[0].src);	
				}
				if (curIndex > 0) {
					$('.prev-img').attr('src', images[curIndex - 1].src);
				}	
				else if(curIndex == 0) {
					$('.prev-img').attr('src', images[images.length - 1].src);
				}					
				$('.prev-img').css('left', '-900px');
			}	
		});
	});
	
	$('.fa-angle-right').click(function(){
		$('.current-img').animate({
			left: '-=900'
		}, {
			duration: 1000,
			complete: function(){
				if (curIndex < images.length - 2) {
					curIndex++;
				}
				else if (curIndex == images.length - 2) {
					curIndex = images.length - 1;
				}
				else if (curIndex == images.length - 1) {
					curIndex = 0;
				}		
				$('.current-img').attr('src', images[curIndex].src);
				$('.current-img').css({left:'0px'});
			}
		});	
		$('.next-img').animate({
			left: '-=900'
		}, {
			duration: 1000,
			complete: function(){	
				if (curIndex < images.length - 1) {
					$('.next-img').attr('src', images[curIndex + 1].src);					
				}
				else if (curIndex == images.length - 1) {
					$('.next-img').attr('src', images[0].src);	
				}
				if (curIndex > 0) {
					$('.prev-img').attr('src', images[curIndex - 1].src);
				}	
				else if(curIndex == 0) {
					$('.prev-img').attr('src', images[images.length - 1].src);
				}			
				$('.next-img').css('left', '900px');
			}
		});			
	});
	
	// modal alert-box
	var alertMessage = $(".contact input[name='alert']").val();
	if (alertMessage == 'Your message has been sent.') {
		$('.modalDialog').css({
			display: 'block',
			pointerEvents: 'auto'
		});
		$('.modalDialog p').text(alertMessage);
	}
	$('.close-modalDialog').click(function() {
		$('.modalDialog').css({
			display: 'none',
			pointerEvents: 'auto'
		});
		$(".contact input[name='alert']").val('');
	});


});
