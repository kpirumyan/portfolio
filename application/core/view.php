<?php 
	class View
	{
		public function generate($view, $data = null) {
			ob_start();
			require_once 'application/views/' . $view;
			echo ob_get_clean();			 	
		}	
	}
?>