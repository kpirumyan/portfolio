<?php
	class Route
	{
		static function start()	{
			// default controller and action
			$controller_name = 'main';
			$action_name = 'index';
			
			// URL fragmentation
			global $site_dir;
			if (!empty($site_dir)) {
        $start_pos = strpos($_SERVER['REQUEST_URI'], $site_dir) + strlen($site_dir) + 1;
        $end_pos = strpos($_SERVER['REQUEST_URI'], '?');
        if ($end_pos !== false)
          $str = substr($_SERVER['REQUEST_URI'], $start_pos, $end_pos - $start_pos);
        else
          $str = substr($_SERVER['REQUEST_URI'], $start_pos);
        $routes = explode('/', $str);
      }

			// set controller name
			if (!empty($routes[0])) {	
				$controller_name = $routes[0];
			}
			
			// set action name
			if (!empty($routes[1])) {
				$action_name = $routes[1];
			}

			// add prefix
			$model_name = 'Model_'.$controller_name;
			$controller_name = 'Controller_'.$controller_name;
			$action_name = $action_name;

			// include model file
			$model_file = strtolower($model_name).'.php';
			$model_path = "application/models/".$model_file;
			if (file_exists($model_path))	{
				include "application/models/".$model_file;
			}

			// include controller file
			$controller_file = strtolower($controller_name).'.php';
			$controller_path = "application/controllers/".$controller_file;		
			if (file_exists($controller_path)) 
				include "application/controllers/".$controller_file;
			else
				Route::ErrorPage404();
			
			// create controller
			$controller_name;
			$controller = new $controller_name;
			$action = $action_name;
			
			if (method_exists($controller, $action)) {
				// start action of controller
				$controller->$action();
			}
			else
				Route::ErrorPage404();
		}
		
		function ErrorPage404()	{
			$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			header('HTTP/1.1 404 Not Found');
			header("Status: 404 Not Found");
			header('Location:'.$host.'404');
		}
	}
?>