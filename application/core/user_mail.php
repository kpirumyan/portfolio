<?php 
	class UserMail
	{		
		private $name;
		private $email;
		private $phone;
		private $message;
		
		function get_name(){
			return $this->name;
		}
		
		function set_name($value){
			$this->name = $value;
		}
		
		function get_email(){
			return $this->email;
		}
		
		function set_email($value){
			$this->email = $value;
		}
		
		function get_phone(){
			return $this->phone;
		}
		
		function set_phone($value){
			$this->phone = $value;
		}
		
		function get_message(){
			return $this->message;
		}
		
		function set_message($value){
			$this->message = $value;
		}
	}
?>