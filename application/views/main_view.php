<?php 
	global $site_dir;
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>Karen Pirumyan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <?php		
		echo "
			<link rel='shortcut icon' href='{$site_dir}/favicon.ico' type='image/x-icon'>
			<link href='{$site_dir}/css/style.css' rel='stylesheet' >
			<script src='{$site_dir}/js/jquery-3.2.1.min.js'></script>
			<script src='{$site_dir}/js/QTransform/QTransform.js'></script>
			<script src='{$site_dir}/js/main.js'></script>
		";
	?>
	
</head>
<body class='main'>
	<div class='container'>
		<a href='#home' ><i id='moveUp' class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
		<div class='row home' id='home'>
			<div class="user-icon"></div>
			<h1>Karen Pirumyan</h1>
			<p class='primary-color home-text-style'>Programmer / Web Developer</p>
			<p><textarea id='txt' disabled></textarea></p>	
			<div class='links'>
				<ul>
					<li><a href='https://www.linkedin.com/in/karen-pirumyan-613628146/' target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href='https://www.facebook.com/karen.pirumyan81' target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href='https://vk.com/id34296043' target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class='radio-btns'>		
				<div>		
					<input type='radio' name='home-btns' id='first' />
					<label for="first"></label>
					<input type='radio' name='home-btns' id='second' />
					<label for="second"></label>
					<input type='radio' name='home-btns' id='third' />
					<label for="third"></label>
				</div>
			</div>
		</div>
		<div class='row menu m-big'>
			<h2>RESUME</h2>
			<ul>
				<?php
					$testArray = array(
						'home' => array(
							'title' => "home",
							'path' => "home",
							'access' => 0
						),
						'about' => array(
							'title' => "about",
							'path' => "about",
							'access' => 0
						),	
						'services' => array(
							'title' => "services",
							'path' => "services",
							'access' => 0
						),
						'skills' => array(
							'title' => "skills",
							'path' => "skills",
							'access' => 0
						),
						'gallery' => array(
							'title' => "gallery",
							'path' => "gallery",
							'access' => 0
						),
						'contact' => array(
							'title' => "contact",
							'path' => "contact",
							'access' => 0
						)				
					);
					foreach ($testArray as $p => $title) {
						echo "<li><a href='#{$p}' class='{$p}-link'>{$title['title']}</a></li>";
					}
				?>	
			</ul>
		</div>
		<div class='row menu m-small'>
			<div>
				<h2>RESUME</h2>
				<a><i id='menu_btn' class="fa fa-bars" aria-hidden="true"></i></a>
			</div>
			<ul id='menu_ul'>
				<?php
					$testArray = array(
						'home' => array(
							'title' => "home",
							'path' => "home",
							'access' => 0
						),
						'about' => array(
							'title' => "about",
							'path' => "about",
							'access' => 0
						),	
						'services' => array(
							'title' => "services",
							'path' => "services",
							'access' => 0
						),
						'skills' => array(
							'title' => "skills",
							'path' => "skills",
							'access' => 0
						),
						'gallery' => array(
							'title' => "gallery",
							'path' => "gallery",
							'access' => 0
						),
						'contact' => array(
							'title' => "contact",
							'path' => "contact",
							'access' => 0
						)				
					);
					foreach ($testArray as $p => $title) {
						echo "<li><a href='#{$p}' class='{$p}-link'>{$title['title']}</a></li>";
					}
				?>	
			</ul>
		</div>
		<div class='about' id='about'>
			<div class='row title'>
				<h2>ABOUT ME</h2>
				<p>A FEW WORDS ABOUT ME</p>
			</div >
			<div class='row about-info'>
				<div class='five columns user-icon-2-wrapper'>
					<img class="user-icon-2" src='<?php echo $site_dir ?>/images/user-icon-3.jpg'>
				</div>
				<div class='seven columns'>
					<ul>
						<li>
							<h4>JOB TITLE</h4>
							<p>Programmer / Web Developer</p>
						</li>
						<li>
							<h4>NAME</h4>
							<p>Karen Pirumyan</p>
						</li>
						<li>
							<h4>SEX</h4>
							<p>Male</p>
						</li>
						<li>
							<h4>ADDRESS</h4>
							<p>Wesele 32/4, 30-127, Krakow, Poland</p>
						</li>
						<li>
							<h4>EMAIL ADDRESS</h4>
							<p class='mail-color'>k.pirumyan@gmail.com</p>
						</li>
					</ul>
				</div>
			</div>	
		</div>		
		<div class='services' id='services'>
			<div class='row title'>
				<h2>What I do for you</h2>
				<p>See my services</p>
			</div>
			<div class='row'>
				<div class='four columns'>
					<i class="fa fa-cog" aria-hidden="true"></i>
					<h3>Why choose me</h3>
					<p>Proven track record in Technology</p>
                    <p>Strong leader with international experience, valuing personal integrity and methodical approach</p>
                    <p>Positively challenging existing processes in order to improve quality, having a ‘can do’ approach</p>
                    <p>Dedicated change professional, passionate for digitization, who continually develops himself and others</p>    
					</p>
				</div>
				<div class='four columns'>
					<i class="fa fa-laptop" aria-hidden="true"></i>
					<h3>What I do</h3>
					<p>Fullstack WEB development using PHP, Javascript, HTML, CSS, etc. (see below in 'Technical Skills' section)</p>
					<p>Programming C#</p>
					<p>Working with virtual disbursed teams</p>
				</div>
				<div class='four columns'>
					<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
					<h3>My mission</h3>
					<p>My mission is adding value to the business of the company through objective driven digital design solutions by providing original designs, making business efficient and effective</p>
				</div>
			</div>
		</div>
		<div class='skills' id='skills'>			
			<div class='row container-inner first-inner'>
				<div class='my-biography'>					
					<h2>Academic Qualifications</h2>
                    <div class="education-wrapper">
                        <div class='years'>
                            <h4>2018-2019</h4>
                        </div>
                        <div>
                            <h4>Armenian Code Academy</h4>
                            <ul>
                                <li>Learn C# Programming</li>
                            </ul>
                        </div>
                    </div>
                    <div class="education-wrapper">
                        <div class='years'>
                            <h4>1998-2004</h4>
                        </div>
                        <div>
                            <h4>Russian State Medical University</h4>
                            <ul>
                                <li>Medical Faculty</li>
                                <li>Specialty: doctor</li>
                                <li>Specialization: medical business</li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div class='my-skills'>
					<h2>Technical skills</h2>
					<ul>
						<li data-percent='95%'>
							<div class='skillbar-title'>C# (ASP.NET MVC, WPF, Prism, ADO.NET Entity Framework)</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>
						<li data-percent='91%'>
							<div class='skillbar-title'>JavaScript (jQuery, AJAX)</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>
                        <li data-percent='93%'>
                            <div class='skillbar-title'>PHP (Symfony)</div>
                            <div class='skillbar'><div class='skillbar-bar'></div></div>
                        </li>
						<li data-percent='98%'>
							<div class='skillbar-title'>HTML</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>
						<li data-percent='96%'>
							<div class='skillbar-title'>CSS, SASS</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>

						<li data-percent='90%'>
							<div class='skillbar-title'>Databases (MySQL, MsSQL, PostgreSQL, Coachbase(NoSQL))</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>
						<li data-percent='95%'>
							<div class='skillbar-title'>Drupal 7, 8</div>
							<div class='skillbar'><div class='skillbar-bar'></div></div>
						</li>
                        <li data-percent='95%'>
                            <div class='skillbar-title'>Git</div>
                            <div class='skillbar'><div class='skillbar-bar'></div></div>
                        </li>
                        <li data-percent='50%'>
                            <div class='skillbar-title'>Microsoft Office VBA</div>
                            <div class='skillbar'><div class='skillbar-bar'></div></div>
                        </li>
                        <li data-percent='50%'>
                            <div class='skillbar-title'>Photoshop</div>
                            <div class='skillbar'><div class='skillbar-bar'></div></div>
                        </li>
					</ul>						
				</div>
			</div>			
			<div class='row container-inner second-inner'>
				<div class='my-skills'>						
					<h2>Language skills</h2>
					<ul>
						<li>
							<div class='circle'>			
								<div class='circle layer z0'></div>
								<div class='circle layer z1'></div>
								<div class='circle layer z2'></div>
								<div class='circle layer z3'>100%</div>			
							</div>
							<p>Armenian</p>
						</li>
						<li>
							<div class='circle'>			
								<div class='circle layer z0'></div>
								<div class='circle layer z1'></div>
								<div class='circle layer z2'></div>
								<div class='circle layer z3'>100%</div>			
							</div>
							<p>Russian</p>
						</li>
						<li>
							<div class='circle'>			
								<div class='circle layer z0'></div>
								<div class='circle layer z1'></div>
								<div class='circle layer z2'></div>
								<div class='circle layer z3'>70%</div>			
							</div>
							<p>English</p>
						</li>
					</ul>
				</div>					
				<div class='my-biography'>					
					<h2>Professional experience</h2>
                    <div class='experience experience-1'>
                        <div class='years'>
                            <h4>2018-present</h4>
                            <p>Web-developer</p>
                        </div>
                        <div>
                            <h4>Vectus</h4>
                            <ul>
                                <li>Develop websites using PHP (Symfony, Drupal 7, 8), Javascript, HTML, CSS, MySQL, Git</li>
                                <li>Site deployment</li>
                                <li>Work with virtual disbursed teams</li>
                            </ul>
                        </div>
                    </div>
                    <div class='experience experience-2'>
                        <div class='years'>
                            <h4>2017-2018</h4>
                            <p>Web-developer</p>
                        </div>
                        <div>
                            <h4>Ectostar</h4>
                            <ul>
                                <li>Develop websites using PHP (Symfony, Drupal 7, 8), Javascript, HTML, CSS, MySQL, Git</li>
                                <li>Site deployment</li>
                                <li>Work with virtual disbursed teams</li>
                            </ul>
                        </div>
                    </div>
					<div class='experience experience-3'>
						<div class='years'>
							<h4>2013-2017</h4>
							<p>Head of Lenovo service center in Armenia</p>
						</div>
						<div>
							<h4>LANS LLC</h4>
							<ul>
                                <li>Work with clients and partners</li>
                                <li>Producing monthly financial reports and Lenovo warranty repairs reports</li>
                                <li>Regular conferences with the regional management and other ASCs</li>
                                <li>Experience sharing / onboarding & training of the new team members</li>
                            </ul>
						</div>
					</div>
					<div class='experience experience-4'>
						<div class='years'>
							<h4>2009-2013</h4>
							<p>Engineer of the service department</p>
						</div>
						<div>
							<h4>LANS LLC</h4>
							<ul>
								<li>Diagnostics and repair of laptops, PCs and tablets</li>
								<li>Installation of OS, flashing tablets, virus protection</li>
							</ul>
						</div>
					</div>						
				</div>
			</div>
		</div>
		<div class='gallery' id='gallery'>
			<div class='row title'>
				<h2>My gallery</h2>				
				<p>See my works</p>
			</div>
			<div class='photoviewer'>
				<div class='back'>
					<a class='slide-btns prev'><i class="fa fa-angle-left colour" aria-hidden="true"></i></a>	
					<a class='slide-btns next'><i class="fa fa-angle-right colour" aria-hidden="true"></i></a>
					<a class='close-btn colour'><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>				
				<div class='p-layer front'> 		
					<div class='p-layer view-border'></div>			
					<img class='p-layer prev-img'>
					<img class='p-layer current-img'>
					<img class='p-layer next-img'>					
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/1.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/2.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/3.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/4.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/5.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/6.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/7.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/8.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/9.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/10.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/11.png" ?>'>
					<img class='images' src='<?php echo "{$site_dir}/images/gallery/12.png" ?>'>
				</div>
			</div>
			<div class='row thumbnails'>
				<a><img src='<?php echo "{$site_dir}/images/gallery/1.png" ?>' alt='1_image' data-index='0'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/2.png" ?>' alt='2_image' data-index='1'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/3.png" ?>' alt='3_image' data-index='2'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/4.png" ?>' alt='4_image' data-index='3'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/5.png" ?>' alt='5_image' data-index='4'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/6.png" ?>' alt='6_image' data-index='5'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/7.png" ?>' alt='7_image' data-index='6'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/8.png" ?>' alt='8_image' data-index='7'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/9.png" ?>' alt='9_image' data-index='8'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/10.png" ?>' alt='10_image' data-index='9'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/11.png" ?>' alt='11_image' data-index='10'></a>
				<a><img src='<?php echo "{$site_dir}/images/gallery/12.png" ?>' alt='12_image' data-index='11'></a>
			</div>
        </div>

		<div class='row more'>
			<div>
				<i class="fa fa-user" aria-hidden="true"></i>
				<h4>Clients</h4>
				<p class="counter-value" data-count="9">0</p>
			</div>
			<div>
				<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
				<h4>Projects</h4>
				<p class="counter-value" data-count="9">0</p>
			</div>
			<div>
				<i class="fa fa-heart-o" aria-hidden="true"></i>
				<h4>Experience</h4>
				<p class="counter-value" data-count="3">0</p>
			</div>
			<div>
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<h4>Lines of code</h4>
				<p class="counter-value" data-count="150608">0</p>
			</div>
		</div>
		<div class='contact' id='contact'>
			<div class='row title'>
				<h2>Contact me</h2>
				<p>Send me a message</p>
			</div>
			<div class='row'>
				<form method='post'>
					<div class='one-half column'>
						<input type='text' name='name' placeholder='Your Name' required>
						<input type='email' name='email' placeholder='your-email@example.com' required>
						<input type='text' name='phone' placeholder='Your Phone Number' required>
						<input type='hidden' name='alert' value='<?php echo $data ?>'>
					</div>
					<div class='one-half column'>
						<textarea name='message' placeholder='Your Message'></textarea>
						<input type='submit' value='Submit'>
					</div>
				</form>				
			</div>
		</div>
		<div id="openModal" class="modalDialog">
			<div>
					<a href='#contact' title="Закрыть" class="close-modalDialog">X</a>
					<p></p>
			</div>
		</div>
		<footer class='row'>
			<div class='four columns'>
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				<h4>Location</h4>
				<p>Wesele 32/4, 30-127, Krakow, Poland</p>
			</div>
			<div class='four columns'>
				<i class="fa fa-envelope" aria-hidden="true"></i>
				<h4>Email</h4>
				<p>k.pirumyan@gmail.com</p>
			</div>
			<div class='four columns'>
				<i class="fa fa-phone" aria-hidden="true"></i>
				<h4>Call me</h4>
				<p class="first-number">(+48) 798 055 512</p>
				<p>(+48) 507 425 214</p>
			</div>
		</footer>		
	</div>
</body>
</html>