<?php 
	class Controller_Main extends Controller
	{
		private $usermail;
		
		public function __construct(){
			parent::__construct();
		}
		
		public function index(){	
			if (isset($_POST['name'])) {
				require_once "application/core/user_mail.php";
				$this->usermail = new UserMail();
				$this->usermail->set_name(htmlentities($_POST['name']));
				$this->usermail->set_email(htmlentities($_POST['email']));
				$this->usermail->set_phone(htmlentities($_POST['phone']));
				$this->usermail->set_message(htmlentities($_POST['message']));
				
				$to = 'pir-karen@yandex.ru';
				$subject = 'From portfolio - user: '.$this->usermail->get_name();
				$message = $this->usermail->get_phone() . "\r\n" .
				  $this->usermail->get_message();
				$headers  = 'From: '.$this->usermail->get_name().'<'.$this->usermail->get_email().'>'."\r\n". 
										'Reply-To: '.$this->usermail->get_email()."\r\n".
										'MIME-Version: 1.0' . "\r\n" .
										'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
										'X-Mailer: PHP/' . phpversion();
				
				if(mail($to, $subject, $message, $headers))
						$this->view->generate('main_view.php', 'Your message has been sent.');	
				else
						echo "Email sending failed";
			}
			else {
				$this->view->generate('main_view.php');
			}	
		}
	}
?>